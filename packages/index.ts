import { App } from "vue";
import TButton from "./TButton";
import TBar from "./TBar";
import TAvatar from "./TAvatar";
import TCheckbox from "./TCheckbox";
import TChip from "./TChip";
import TCircle from "./TCircle";
import TClick from "./TClick";
import TForm from "./TForm";
import TSwitch from "./TSwitch";
import TIcon from "./TIcon";
import TRadio from "./TRadio";
import TSlider from "./TSlider";
import TSelect from "./TSelect";
// 所有组件列表
const components = [
  TButton,
  TBar,
  TAvatar,
  TCheckbox,
  TChip,
  TCircle,
  TClick,
  TForm,
  TSwitch,
  TIcon,
  TRadio,
  TSlider,
  TSelect,
];

const getFileName = (comment: any) => {
  return comment.__file
    ?.split("/")
    [comment.__file?.split("/").length - 1].split(".")[0];
};

// 定义 install 方法， App 作为参数
const install = (app: App): void => {
  // 遍历注册所有组件
  components.map((component) =>
    app.component(getFileName(component), component)
  );
};

export {
  TButton,
  TBar,
  TAvatar,
  TCheckbox,
  TChip,
  TCircle,
  TClick,
  TForm,
  TSwitch,
  TIcon,
  TRadio,
  TSlider,
  TSelect,
};

export default {
  install,
};
