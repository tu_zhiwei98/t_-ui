export interface IInputSize {
  mini: string;
  small: string;
  medium: string;
}

// export enum INPUT_SIZE {
//   mini,
//   small,
//   medium
// }
export interface IInputDataItem<T> {
  id: number;
  value: T;
  label: T;
}
