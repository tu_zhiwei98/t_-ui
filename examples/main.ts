import { createApp } from "vue";
import App from "./App.vue";
import T_UI from "../packages/index";

import "../packages/css/base.less";
import "animate.css";

const app = createApp(App);
app.use(T_UI);
app.mount("#app");
